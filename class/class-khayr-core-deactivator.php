<?php

/**
 * Fired during plugin deactivation
 *
 * @link       adi64bit@gmail.com
 * @since      1.0.0
 *
 * @package    KhayrCore
 * @subpackage KhayrCore/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    KhayrCore
 * @subpackage KhayrCore/includes
 * @author     Adi <adi64bit@gmail.com>
 */
class KhayrCore_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
