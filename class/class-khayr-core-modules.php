<?php
/**
 * Class KhayrCore_Modules
 * register all the admin functionality
 *
 * @author AdiSaputra <adi64bit@gmail.com>
 * @version 1.0
 *
 * @param - -
 */
class KhayrCore_Modules {

    public static $instance;

    /**
     * construct
     *
     * @return null
     */
    public function __construct(){
        $this->khayr_module_loader();
    }

    /**
     * instance
     *
     * @return instance
     */
    public static function instance() {
        if ( is_null( self::$instance ) ) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function khayr_module_loader() {
        /*
        * Load module dynamically
        */
        
        $path = KHAYR_CORE_MODULE;
        $namespace = 'KhayrCore_Module_';
        KhayrCore_Helpers::class_loader_dir($path, $namespace);
    }
}