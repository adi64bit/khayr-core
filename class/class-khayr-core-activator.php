<?php

/**
 * Fired during plugin activation
 *
 * @link       adi64bit@gmail.com
 * @since      1.0.0
 *
 * @package    KhayrCore
 * @subpackage KhayrCore/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    KhayrCore
 * @subpackage KhayrCore/includes
 * @author     Adi <adi64bit@gmail.com>
 */
class KhayrCore_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
