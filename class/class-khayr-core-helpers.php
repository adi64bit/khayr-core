<?php
/**
 * A class of helper functions that can be reused across sites.
 *
 * Can be called in any templates file, `KhayrCore_Helpers::is_dev()`
 */

class KhayrCore_Helpers
{
    public static function is_ajax()
    {
        return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }

    /**
	 * This will evaluate wheter a URL is at an aboslute location (like http://example.org/whatever)
	 *
	 * @param string $path
	 * @return boolean true if $path is an absolute url, false if relative.
	 */
	public static function is_absolute( $path ) {
		return (boolean) (strstr($path, 'http'));
	}


	/**
	 * This function is slightly different from the one below in the case of:
	 * an image hosted on the same domain BUT on a different site than the
	 * Wordpress install will be reported as external content.
	 *
	 * @param string  $url a URL to evaluate against
	 * @return boolean if $url points to an external location returns true
	 */
	public static function is_external_content( $url ) {
		$is_external = self::is_absolute($url) && !self::is_internal_content($url);

		return $is_external;
	}

    /**
	 * @param string $url
	 */
	private static function is_internal_content( $url ) {
		// using content_url() instead of site_url or home_url is IMPORTANT
		// otherwise you run into errors with sites that:
		// 1. use WPML plugin
		// 2. or redefine content directory
		$is_content_url = strstr($url, content_url());

		// this case covers when the upload directory has been redefined
		$upload_dir = wp_upload_dir();
		$is_upload_url = strstr($url, $upload_dir['baseurl']);

		return $is_content_url || $is_upload_url;
	}

	public static function class_loader_dir($path, $namespace){
		$dir = new \DirectoryIterator($path);
        
        foreach ($dir as $dirinfo) {
            
            if (!$dirinfo->isDot()) {
                $filename = $dirinfo->getFilename();
                $filename = str_replace('.php', '', $filename);
                include($path.$dirinfo.'/'.$dirinfo.'.php');
                $class = "{$namespace}{$filename}";
				if (class_exists($class)) {
					
					new $class();
				}
            }
        }
	}

	public static function class_loader_file($path, $namespace){
		$dir = new \DirectoryIterator($path);

        foreach ($dir as $dirinfo) {

            if (!$dirinfo->isDot()) {
                $filename = $dirinfo->getFilename();
                $filename = str_replace('.php', '', $filename);

                $class = "{$namespace}\\{$filename}";
				if (\class_exists($class)) {
					new $class();
				}
            }
        }
	}
}
