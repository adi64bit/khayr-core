<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       adi64bit@gmail.com
 * @since      1.0.0
 *
 * @package    KhayrCore
 * @subpackage KhayrCore/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    KhayrCore
 * @subpackage KhayrCore/includes
 * @author     Adi <adi64bit@gmail.com>
 */
class KhayrCore {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      KhayrCore_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'KHAYR_CORE_VERSION' ) ) {
			$this->version = KHAYR_CORE_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'khayr-core';

		$this->load_dependencies();
		$this->setup();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {
		// load vendor
		require_once KHAYR_CORE . '/vendor/cmb2/init.php';
		// load class
		require_once KHAYR_CORE . 'class/class-khayr-core-i18n.php';
		require_once KHAYR_CORE . 'class/class-khayr-core-admin.php';
		require_once KHAYR_CORE . 'class/class-khayr-core-helpers.php';
		require_once KHAYR_CORE . 'class/class-khayr-core-modules.php';
	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the KhayrCore_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function setup() {
		if (class_exists('KhayrCore_Admin')) {
			new KhayrCore_Admin();
		}

		if (class_exists('KhayrCore_Modules')) {
			new KhayrCore_Modules();
		}

		add_filter( 'khayr_admin_script_loader', function($js){
            $js[] = [
                'handle'          => 'cmb2-typography-field',
                'footer'          => true,
				'plugin'		=> true,
                'src'             => KHAYR_CORE_ASSET.'cmb2-typography.js'
            ];

            return $js;
        });

        add_filter( 'khayr_admin_style_loader', function($css){
            $css[] = [
                'handle'          => 'cmb2-typography-field',
                'footer'          => true,
				'plugin'		=> true,
                'src'             => KHAYR_CORE_ASSET.'cmb2-typography.css'
            ];

            return $css;
        });
		
		$plugin_i18n = new KhayrCore_i18n();
		add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
