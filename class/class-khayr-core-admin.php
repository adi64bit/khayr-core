<?php
/**
 * Class Admin
 * register all the admin functionality
 *
 * @author AdiSaputra <adi64bit@gmail.com>
 * @version 1.0
 *
 * @param - -
 */
class KhayrCore_Admin {

    public static $instance;

    /**
     * construct
     *
     * @return null
     */
    public function __construct(){
        
        // register admin menu
        add_action( 'admin_menu', array( $this, 'register_admin_pages' ) );
        add_action( 'admin_menu', array( $this, 'remove_parent_menu' ) );

        // https://github.com/elementor/elementor/issues/6022
		add_action( 'admin_init', function() {
			if ( did_action( 'elementor/loaded' ) ) {
				remove_action( 'admin_init', [ \Elementor\Plugin::$instance->admin, 'maybe_redirect_to_getting_started' ] );
			}
		}, 1 );
    }

    /**
     * instance
     *
     * @return instance
     */
    public static function instance() {
        if ( is_null( self::$instance ) ) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * register_admin_pages
     *
     * @return void
     */
    public function register_admin_pages(){
        $sub_menus = array();

        add_menu_page(
            esc_html__( 'Khayr Tools', 'khayr' ),
            esc_html__( 'Khayr Tools', 'khayr' ),
            'manage_options',
            'khayr_dashboard',
            '',
            KHAYR_CORE_ASSET.'khayr-logo-32.png',
            '48'
        );

        if (class_exists('KhayrCore_HeaderBuilder')) {
            $sub_menus['khayr_header_builder'] = array(
                'khayr_dashboard',
                esc_html__( 'Header Builder', 'khayr' ),
                esc_html__( 'Header Builder', 'khayr' ),
                'manage_options',
                'khayr_header_builder',
                array( 'KhayrCore_HeaderBuilder', 'render' ),
            );
        }

        if (class_exists('KhayrCore_Module_CPTBuilder')) {
            $sub_menus['khayr_post_types'] = array(
                'khayr_dashboard',
                esc_html__( 'Post Types', 'khayr' ),
                esc_html__( 'Post Types', 'khayr' ),
                'manage_options',
                'khayr_post_types',
                array( 'KhayrCore_Module_CPTBuilder', 'render' ),
            );
        }

	    // Add filter for third party uses
        $sub_menus = apply_filters( 'khayr_admin_sub_menus', $sub_menus, 20 );


        $sub_menus['documentation'] = array(
            'khayr_dashboard',
            esc_html__( 'Documentation', 'khayr' ),
            esc_html__( 'Documentation', 'khayr' ),
            'manage_options',
            'khayr_documentation',
            array( $this, 'documentation' ),
        );

        if ( $sub_menus ) {
            foreach ( $sub_menus as $sub_menu ) {
                call_user_func_array( 'add_submenu_page', $sub_menu );
            }
        }
    }

    /**
     * remove_parent_menu
     *
     * @return void
     */
    public function remove_parent_menu() {
		global $submenu;
		unset( $submenu['khayr_dashboard'][0] );
	}

    /**
     * documentation
     *
     * @return void
     */
    public function documentation() {
		require_once KHAYR_FRAMEWORK.'templates/admin-documentation.php';
	}
}