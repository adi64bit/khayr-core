<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              adi64bit@gmail.com
 * @since             1.0.0
 * @package           KhayrCore
 *
 * @wordpress-plugin
 * Plugin Name:       Khayr - Core
 * Plugin URI:        adi64bit@gmail.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Adi
 * Author URI:        adi64bit@gmail.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       khayr-core
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'KHAYR_CORE_VERSION', '1.0.0' );
define( 'KHAYR_CORE', plugin_dir_path( __FILE__ ) );
define( 'KHAYR_CORE_CLASS', plugin_dir_path( __FILE__ ) . 'class/' );
define( 'KHAYR_CORE_MODULE', plugin_dir_path( __FILE__ ) . 'modules/' );
define( 'KHAYR_CORE_TEMPLATE', plugin_dir_path( __FILE__ ) . 'templates/' );
define( 'KHAYR_CORE_ASSET', plugin_dir_url( __FILE__ ) . 'assets/' );

/**
 * The code that runs during plugin activation.
 * This action is documented in class/class-khayr-core-activator.php
 */
function activate_khayr_core() {
	require_once plugin_dir_path( __FILE__ ) . 'class/class-khayr-core-activator.php';
	KhayrCore_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in class/class-khayr-core-deactivator.php
 */
function deactivate_khayr_core() {
	require_once plugin_dir_path( __FILE__ ) . 'class/class-khayr-core-deactivator.php';
	KhayrCore_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_khayr_core' );
register_deactivation_hook( __FILE__, 'deactivate_khayr_core' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'class/class-khayr-core.php';

