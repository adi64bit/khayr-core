<div class="admin-khayr">
    <div class="admin-khayr__header">
        <div class="admin-khayr__header-logo">
            <img src="<?php echo KHAYR_CORE_ASSET.'khayr-logo.png'; ?>" alt="khayr-logo-white">
        </div>
        <div class="admin-khayr__header-tag"><?php esc_html_e('Current frameworks version', 'khayr'); ?> <?php echo KHAYR_CORE_VERSION; ?></div>
    </div>
    <div class="admin-khayr__nav">
        <ul class="admin-khayr__nav-lists">
            <?php 
                require_once KHAYR_CORE_TEMPLATE.'admin-tabs.php';
            ?>
        </ul>
    </div>
    <div class="admin-khayr__content">