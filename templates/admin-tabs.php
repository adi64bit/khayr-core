<?php 
$current_tab = isset($_GET['page']) ? esc_attr($_GET['page']) : '';

$tabs = array(
	'khayr_header_builder' => esc_html__('Header Builder', 'khayr'),
    'khayr_post_types' => esc_html__('Post Types', 'khayr'),
    'khayr_documentation' => esc_html__('Documentation', 'khayr')
);

foreach( $tabs as $slug => $name ) { 
    $active_tab = ( $current_tab === $slug ) ? ' admin-khayr__nav-item-active' : '';
    ?>
    <li class="admin-khayr__nav-item <?php echo esc_attr($active_tab);?>">
        <a href="?page=<?php echo esc_attr($slug); ?>" class="admin-khayr__nav-link"><?php echo esc_attr($name); ?></a>
    </li>
<?php } ?>