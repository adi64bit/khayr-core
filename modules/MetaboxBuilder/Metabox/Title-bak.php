<?php

class KhayrCore_Metabox_Title extends KhayrCore_MetaboxAbstract
{
    public function khayr_config(){
        return array(
            'id'            => 'khayr_page_title',
            'title'         => esc_html__( 'Khayr Title', 'khayr-admin' ),
            'object_types'  => array('page', 'post'),
            'show_on_cb' => 'khayr_show_if_front_page',
            'context'    => 'normal',
            'priority'   => 'high',
            'show_names' => true,
            'closed'     => false
        );
    }

    public function khayr_options(){
        $options = array();

        $options[] = array(
            'name' => esc_html__( 'Title Area', 'khayr-admin' ),
            'desc' => esc_html__( 'Enable this option to turn off page title area', 'khayr-admin' ),
            'id'   => 'khayr_show_page_title',
            'type' => 'select',
            'show_option_none' => false,
            'default'          => '',
            'options'          => array(
                ''          => esc_html__( 'Default', 'khayr-admin' ),
                'no'        => esc_html__( 'No', 'khayr-admin' ),
                'yes'       => esc_html__( 'Yes', 'khayr-admin' ),
            )
        );

        $options[] = array(
            'name' => esc_html__( 'Text', 'khayr-admin' ),
            'desc' => esc_html__( 'text text', 'khayr-admin' ),
            'id'   => 'khayr_page_text',
            'type' => 'text',
        );
        return $options;
    }
}
