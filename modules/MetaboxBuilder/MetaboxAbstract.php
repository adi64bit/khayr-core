<?php

abstract class KhayrCore_MetaboxAbstract
{
    
	public $config = array(
        'type'          => 'metabox',
		'id'            => 'khayr_demo_metabox',
		'title'         => 'Test Metabox',
		'object_types'  => array( 'page' )
	);

    public $options = array();

    public $metabox = false;

    public function __construct($data = [], $args = null) {
		$config = $this->khayr_config();

        if (!$config && is_array($config)) {
            return;
        }

        $this->config = $this->khayr_pairs_config($this->config, $config);
        $this->options = $this->khayr_options();
        if (!$this->options && !is_array($this->options)) {
            return;
        }

        $this->khayr_init();
	}

    public function khayr_init(){
        add_filter('khayr_'.$this->config['type'].'_register', array( $this, 'khayr_register_config'));
    }

    public function khayr_register_config($config){
        $config[$this->config['id']] = array(
            'config'    => $this->config,
            'options'   => $this->options
        );

        return $config;
    }

    public function khayr_config(){
        return false;
    }

    public function khayr_options(){
        return false;
    }

    public function khayr_pairs_config($pairs, $atts){
		$atts = $atts;
		$out  = array_merge($pairs, $atts);
		return $out;
	}
}
