<?php 

if (!class_exists('KhayrCore_Module_MetaboxBuilder')) {
    class KhayrCore_Module_MetaboxBuilder{

        public static $theme_options        = array();
        public static $theme_metabox        = array();
    
        protected static $post_type = array();
        
        public function __construct(){
            require_once(KHAYR_CORE_MODULE . 'MetaboxBuilder/MetaboxAbstract.php');
            
            $pluginPath = KHAYR_CORE_MODULE . 'MetaboxBuilder/Metabox';
            $this->register_metabox($pluginPath, 'KhayrCore_Metabox_');

            $this->register_filter();          
            add_action( 'cmb2_init', array( $this, 'cmb2_init' ) );
            add_action( 'cmb2_admin_init', array( $this, 'run_metabox' ) );
        }

        public function cmb2_init(){
            require_once KHAYR_CORE . 'class/class-khayr-core-cmb2-typography.php';
		    CMB2_Render_Typography_Field::init();
        }
    
        public function register_filter(){
            static::$theme_metabox          = apply_filters( 'khayr_metabox_register', array());
            static::$theme_options          = apply_filters( 'khayr_options_register', array());
        }
    
        public function register_metabox($path, $class_name){
            $dir = new \DirectoryIterator($path);
            foreach ($dir as $dirinfo) {
                if (!$dirinfo->isDot()) {
                    $filename = $dirinfo->getFilename();
                    include($path.'/'.$filename);
                    $filename = str_replace('.php', '', $filename);
                    $class = "{$class_name}{$filename}";
                    if (\class_exists($class)) {
                        new $class();
                    }
                }
            }
        }
    
        public function run_metabox(){
            $this->register_filter();
            
            foreach (static::$theme_options as $key => $value) {
                static::$theme_options[$key]['call'] = new_cmb2_box($value['config']);
                foreach ($value['options'] as $k => $v) {
                    $fields = false;
                    if ($v['type'] == 'group' && isset($v['fields'])) {
                        $fields = $v['fields'];
                        unset($v['fields']);
                    }

                    $field_id = static::$theme_options[$key]['call']->add_field($v);

                    if ($v['type'] == 'group' && $fields) {
                        foreach ($fields as $kk => $vv) {
                            static::$theme_options[$key]['call']->add_group_field($field_id, $vv);
                        }
                    }
                }
            }

            foreach (static::$theme_metabox as $key => $value) {
                static::$theme_metabox[$key]['call'] = new_cmb2_box($value['config']);
                foreach ($value['options'] as $k => $v) {
                    static::$theme_metabox[$key]['call']->add_field($v);
                }
            }
        }
    }
}
?>