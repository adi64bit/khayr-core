<?php

class KhayrCore_CPT_Team extends KhayrCore_CPT_CPTAbstract
{
    protected static $postType = 'team';

    protected static $postName = 'Khayr Team';

    protected static $singularName = 'Khayr Team';

    protected static $pluralName = 'Khayr Teams';

    protected static $supports = array(
        'title',
        'thumbnail',
        'excerpt',
        'revisions'
    );
}
