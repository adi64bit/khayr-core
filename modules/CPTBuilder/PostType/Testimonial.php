<?php

class KhayrCore_CPT_Testimonial extends KhayrCore_CPT_CPTAbstract
{
    protected static $postType = 'testimonial';

    protected static $postName = 'Khayr Testimonial';

    protected static $singularName = 'Khayr Testimonial';

    protected static $pluralName = 'Khayr Testimonials';

    protected static $supports = array(
        'title',
        'thumbnail',
        'excerpt',
        'revisions'
    );
}
