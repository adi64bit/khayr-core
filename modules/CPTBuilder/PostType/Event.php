<?php

class KhayrCore_CPT_Event extends KhayrCore_CPT_CPTAbstract
{
    protected static $postType = 'event';

    protected static $postName = 'Khayr Event';

    protected static $singularName = 'Khayr Event';

    protected static $pluralName = 'Khayr Events';

    protected static $supports = array(
        'title',
        'thumbnail',
        'excerpt',
        'revisions'
    );
}
