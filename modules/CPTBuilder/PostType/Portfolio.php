<?php

class KhayrCore_CPT_Portfolio extends KhayrCore_CPT_CPTAbstract
{
    protected static $postType = 'portfolio';

    protected static $postName = 'Khayr Portfolio';

    protected static $singularName = 'Khayr Portfolio';

    protected static $pluralName = 'Khayr Portfolios';

    protected static $supports = array(
        'title',
        'thumbnail',
        'excerpt',
        'revisions'
    );
}
