<?php

class KhayrCore_CPT_HeaderBuilder extends KhayrCore_CPT_CPTAbstract
{
    protected static $postType = 'header_builder';

    protected static $postName = 'Header Builder';

    protected static $singularName = 'Header Builder';

    protected static $pluralName = 'Header Builder';

    protected static $showInMenu = false;

    protected static $public = false;

    protected static $active = true;

    protected static $hasArchive = false;

    protected static $supports = array(
        'title',
        'thumbnail',
        'excerpt',
        'revisions'
    );
}
