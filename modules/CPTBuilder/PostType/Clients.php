<?php
class KhayrCore_CPT_Clients extends KhayrCore_CPT_CPTAbstract
{
    protected static $postType = 'clients';

    protected static $postName = 'Khayr Client';

    protected static $singularName = 'Khayr Client';

    protected static $pluralName = 'Khayr Clients';

    protected static $supports = array(
        'title',
        'thumbnail',
        'excerpt',
        'revisions'
    );
}
