<?php 
/**
 * @namespace Khayr\Modules
 */

class KhayrCore_Module_CPTBuilder{

    protected static $post_type = array();
    
    public function __construct(){
        require_once(KHAYR_CORE_MODULE . 'CPTBuilder/CPTAbstract.php');
        self::register_post_type();

        add_action( 'admin_init', array( $this, 'khayr_register_settings' ), 11);

        self::active_post_type();
    }

    public static function register_post_type($filter = 'init', $priority = 10){
        add_action($filter, function(){
            $dir = new \DirectoryIterator( KHAYR_CORE_MODULE . 'CPTBuilder/PostType');

            foreach ($dir as $dirinfo) {

                if (!$dirinfo->isDot() && !in_array($dirinfo->getFilename(), ['CustomPostType.php', 'Example.php']) ) {
                    $filename = $dirinfo->getFilename();

                    include(KHAYR_CORE_MODULE . 'CPTBuilder/PostType/'.$filename);
                    
                    $filename = str_replace('.php', '', $filename);
                    
                    $class = "KhayrCore_CPT_{$filename}";
                    if (\class_exists($class)) {
                        self::$post_type[] = array(
                            'name'  => $filename,
                            'setting'   => 'khayr_'.$filename.'_post',
                            'class' => $class,
                            'active'    => $class::isActive(),
                            'public'    => $class::isPublic()
                        );
                    }
                }
            }
        }, $priority);
    }

    public static function active_post_type($filter = 'init', $priority = 12){
        add_action($filter, function(){
            if (self::$post_type) {
                foreach (self::$post_type as $key => $value) {
                    $active = self::get_setting($value['setting']);
                    if ($active == 'enabled') {
                        $value['class']::register();
                    }
                }
            }
        }, $priority);
    }

    public static function activated_post_type($args){

        $args = shortcode_atts(array(
            'postType'     => false,
            'postName'     => 'Post Name',
            'pluralName'   => 'Posts Name',
            'singularName'  => 'Post Name',
            'supports'      => array(
                'title',
                'editor',
                'thumbnail',
                'page-attributes',
                'excerpt'
            ),
            'active' => false,
            'hierarchical' => false,
            'public' => true,
            'showInMenu' => true,
            'showInNavMenus' => false,
            'hasArchive' => true,
            'excludeFromSearch' => false,
            'publiclyQueryable' => true,
            'menuIcon' => 'dashicons-index-card'
        ), $args);

        if ($args && $args['postType']) {
            register_post_type( $args['postType'], [
                'label'               => __( $args['postName'], 'text_domain' ),
                'labels'              => array(
                    'name'                => _x( $args['pluralName'], 'Post Type General Name', 'text_domain' ),
                    'singular_name'       => _x( $args['singularName'], 'Post Type Singular Name', 'text_domain' ),
                    'menu_name'           => __( $args['pluralName'], 'text_domain' ),
                    'name_admin_bar'      => __( $args['pluralName'], 'text_domain' ),
                    'add_new_item'        => __( 'Add New ' . $args['singularName'], 'text_domain' ),
                    'add_new'             => __( 'Add New ' . $args['singularName'], 'text_domain' ),
                    'new_item'            => __( 'New ' . $args['singularName'], 'text_domain' ),
                    'edit_item'           => __( 'Edit ' . $args['singularName'], 'text_domain' ),
                    'update_item'         => __( 'Update ' . $args['singularName'], 'text_domain' ),
                    'view_item'           => __( 'View ' . $args['singularName'], 'text_domain' ),
                    'search_items'        => __( 'Search ' . $args['pluralName'], 'text_domain' ),
                ),
                'supports'            => $args['supports'],
                'hierarchical'        => $args['hierarchical'],
                'public'              => $args['isPublic'],
                'show_ui'             => true,
                'show_in_menu'        => $args['showInMenu'],
                'menu_position'       => 10,
                'show_in_admin_bar'   => true,
                'show_in_nav_menus'   => $args['showInNavMenus'],
                'can_export'          => true,
                'has_archive'         => $args['hasArchive'],
                'exclude_from_search' => $args['excludeFromSearch'],
                'publicly_queryable'  => $args['publiclyQueryable'],
                'capability_type'     => 'post',
                'menu_icon'           => $args['menuIcon'],
            ] );
        }
    }

    public static function render(){
        require_once KHAYR_CORE_TEMPLATE.'admin-header.php';
        // Flush the rewrite rules if the settings were updated.
        if ( isset( $_GET['settings-updated'] ) )
            flush_rewrite_rules(); ?>

            <div class="khayr-admin-wrapper">

                <?php settings_errors(); ?>
                <div class="admin-khayr-content">
                    <form method="post" action="options.php">
                        <?php settings_fields( 'khayr_ptype_settings' ); ?>
                        <?php do_settings_sections( 'khayr_post_types' ); ?>
                        <?php submit_button( esc_attr__( 'Update Settings', 'khayr-admin' ), 'primary' ); ?>
                    </form>
                </div>

            </div><!-- wrap -->
        <?php
        require_once KHAYR_CORE_TEMPLATE.'admin-footer.php';
    }

    public static function khayr_register_settings() {

        // Register the setting.
        register_setting( 'khayr_ptype_settings', 'khayr_ptype_settings', array( __CLASS__, 'khayr_validate_settings' ) );

        /* === Settings Sections === */
        add_settings_section( 'khayr_post_types_section', esc_html__( 'Custom Post Types', 'khayr-admin' ), array( __CLASS__, 'khayr_section_post_types' ), 'khayr_post_types' );

        /* === Settings Fields === */
        if (self::$post_type) {
            foreach (self::$post_type as $key => $value) {
                if ($value['public']) {
                    add_settings_field( $value['setting'],   esc_html__( $value['name'],   'khayr-admin' ), function($item) use ($value){
                        ?>
                            <label>
                                <select name="khayr_ptype_settings[<?php echo $value['setting'];?>]" class="regular-text">
                                    <option <?php selected(self::get_setting($value['setting']), 'enabled'); ?> value="enabled"><?php esc_html_e('Enabled', 'khayr'); ?></option>
                                    <option <?php selected(self::get_setting($value['setting']), 'disabled'); ?> value="disabled"><?php esc_html_e('Disabled', 'khayr'); ?></option>
                                </select>
                            </label>
                        <?php
                    }, 'khayr_post_types', 'khayr_post_types_section' );
                }
            }
        }
    }

    /**
     * Taxonomies section callback.
     *
     * @since  1.0.0
     * @access public
     * @return void
     */
    public static function khayr_section_post_types() { ?>

        <p class="description">
            <?php esc_html_e( 'Disable Custom Post Types which you do not want to show(if disabled then these will not show on back-end and front-end)', 'khayr-admin' ); ?>
        </p>
    <?php }

    /**
     * Validates the plugin settings.
     *
     * @since  1.0.8
     * @access public
     * @param  array  $input
     * @return array
     */
    public static function khayr_validate_settings( $settings ) {

        if (self::$post_type) {
            foreach (self::$post_type as $key => $value) {
                // Text boxes.
                $settings[$value['setting']] = $settings[$value['setting']] ? trim( strip_tags( $settings[$value['setting']]   ), '/' ) : '';
            }
        }

        // Return the validated/sanitized settings.
        return $settings;
    }

    /**
     * Returns taxonomy settings.
     *
     * @since  1.0.8
     * @access public
     * @param  string  $setting
     * @return mixed
     */
    public static function get_setting( $setting ) {
        $defaults = array();
        if (self::$post_type) {
            foreach (self::$post_type as $key => $value) {
                $defaults[$value['setting']] = $value['active'] ? 'enabled' : 'disabled';
            }
        }
        $settings = wp_parse_args( get_option('khayr_ptype_settings', $defaults ), $defaults );

        return isset( $settings[ $setting ] ) ? $settings[ $setting ] : false;
    }
}

?>